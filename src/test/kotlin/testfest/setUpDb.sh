docker exec testfest_db psql -h localhost -U testfest_user postgres \
  -c \
  "SELECT pg_terminate_backend(pg_stat_activity.pid)
    FROM pg_stat_activity
    WHERE pg_stat_activity.datname = 'testfest'
    AND pid <> pg_backend_pid();" \
  -c "drop database if exists testfest;" \
  -c "create database testfest" \
  -c "\c testfest" \
  -c \
  "CREATE TABLE users (
    id SERIAL PRIMARY KEY,
    username VARCHAR NOT NULL,
    password VARCHAR NOT NULL,
    first_name VARCHAR NOT NULL,
    middle_name VARCHAR NOT NULL,
    last_name VARCHAR NOT NULL,
    birth_date VARCHAR NOT NULL
);

CREATE TABLE roles (
    id SERIAL PRIMARY KEY,
    name VARCHAR NOT NULL
);

CREATE TABLE user_roles (
    user_id INT REFERENCES users ON DELETE CASCADE,
    role_id INT REFERENCES roles
);" \
  -c \
  "
INSERT INTO users VALUES (0, 'admin', '\$2y\$10\$16ovn5P7szguYN/LNe8ifeZo2.2iS.z5DwfOX4c7i9wRnXJkTRp.O', 'Ivan', 'Ivanovich', 'Ivanov', '1970-01-01');
INSERT INTO users VALUES (1, 'hellouser', '\$2y\$10\$22WrZp1rHw/ZQtJqhckqqOxjZin6pXzGHrASUeWT0pcGq2ZA0MLmu', 'Petr', 'Petro', 'Petrov', '1970-02-02');

INSERT INTO roles VALUES (0, 'USER');
INSERT INTO roles VALUES (1, 'ADMIN');

INSERT INTO user_roles VALUES (0, 0);
INSERT INTO user_roles VALUES (0, 1);
INSERT INTO user_roles VALUES (1, 0);
" \
  -c \
  "ALTER TABLE users ADD UNIQUE (username);" \
  -c "select setval('users_id_seq', (select max(id) from users) + 1);" \
  -c "CREATE TABLE IF NOT EXISTS courses (
    id SERIAL PRIMARY KEY,
    course_name VARCHAR NOT NULL,
    course_description VARCHAR NOT NULL
);

INSERT
INTO courses
    (course_name, course_description)
    VALUES (
            'Frontend-разработчик',
            'Он разрабатывает пользовательский интерфейс, то есть внешнюю публичную часть сайта в браузере. ' ||
            'Главная задача Frontend-разработчика — сделать максимально удобным взаимодействие ' ||
            'пользователей с сайтом или веб-приложением'
           ),
           (
            'Тренер по фитнесу',
            'Инструктор, руководящий тренировками и следящий за правильностью выполнения физических упражнений.'
           ),
           (
            'Backend-разработичик',
            'Его область деятельности - программно-административной частью веб-приложения, внутренним ' ||
            'содержанием системы, серверными технологиями — базой данных, архитектурой, программной логикой.'
           ),
           (
            'Финансовый аналитик',
            'Он принимает активное участие в управлении финансами и их оптимизации. Он оценивает риски ' ||
            'и потенциальную выгоду для каждого конкретного рыночного предложения, помогает выбрать перспективные ' ||
            'направления инвестиций, оценивает финансовое состояние контрагента или рыночной отрасли.'
           );"
