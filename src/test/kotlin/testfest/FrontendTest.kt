package testfest

import org.junit.After
import org.junit.Before
import org.junit.Test
import org.openqa.selenium.By
import org.openqa.selenium.Keys.ENTER
import org.openqa.selenium.chrome.ChromeDriver
import org.openqa.selenium.chrome.ChromeOptions
import org.openqa.selenium.support.ui.ExpectedConditions
import org.openqa.selenium.support.ui.ExpectedConditions.*
import org.openqa.selenium.support.ui.WebDriverWait


class FrontendTest {

    private lateinit var driver: ChromeDriver
    private lateinit var wait: WebDriverWait

    private val login = "hellouser"
    private val password = "qoQzPBVyTrg3Qdjj"

    private val newLogin = "newLogin"
    private val newPassword = "newPassword-0"

    @Before
    fun setUp() {
        val chromeOptions = ChromeOptions()
        chromeOptions.setBinary("/opt/google/chrome/chrome")
        System.setProperty("webdriver.chrome.driver", "/home/dsatyanov/web/chromedriver")
        driver = ChromeDriver(chromeOptions)
        wait = WebDriverWait(driver, 5)
    }

    @After
    fun stop() {
        driver.close()
    }

    @Test
    fun `success login`() {
        driver.get("http://localhost/")
        val loginView = driver.findElement(By.name("login"))
        val passwordView = driver.findElement(By.name("password"))

        loginView.sendKeys(login)
        passwordView.sendKeys(password)
        passwordView.sendKeys(ENTER)

        wait.until { driver.findElementByClassName("header__info").text.contains("Petr Petrov") }
    }

    @Test
    fun `show alert on login without password`() {
        driver.get("http://localhost/")
        val loginView = driver.findElement(By.name("login"))

        loginView.sendKeys(login)
        loginView.sendKeys(ENTER)

        wait.until { driver.switchTo().alert().accept() }
    }

    @Test
    fun `switch to registration`() {
        driver.get("http://localhost/")
        val registerTab = driver.findElementsByClassName("tab").find { it.text.contains("Регистрация") }

        registerTab?.click()

        wait.until { driver.findElementByTagName("button").text.contains("Зарегистрироваться") }
    }

    @Test
    fun `success registration`() {
        driver.get("http://localhost/")
        val registerTab = driver.findElementsByClassName("tab").find { it.text.contains("Регистрация") }

        registerTab?.click()

        val loginView = driver.findElement(By.name("login"))
        val passwordView = driver.findElement(By.name("password"))
        val surnameView = driver.findElement(By.name("surname"))
        val nameView = driver.findElement(By.name("name"))
        val patronymicView = driver.findElement(By.name("patronymic"))
        val dateOfBirthView = driver.findElement(By.name("dateOfBirth"))

        loginView.sendKeys(newLogin)
        passwordView.sendKeys(newPassword)
        surnameView.sendKeys("surnamE")
        nameView.sendKeys("namE")
        patronymicView.sendKeys("patronymiC")
        dateOfBirthView.sendKeys("2222010")
        dateOfBirthView.sendKeys(ENTER)

        wait.until { driver.findElementByClassName("header__info").text.contains("namE surnamE") }
    }

    @Test
    fun `fail registration with empty form`() {
        driver.get("http://localhost/")
        val registerTab = driver.findElementsByClassName("tab").find { it.text.contains("Регистрация") }
        registerTab?.click()
        val button = driver.findElementByTagName("button")
        button.click()
        wait.until { driver.switchTo().alert().accept() }
    }
}