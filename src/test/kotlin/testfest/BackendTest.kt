package testfest

import com.google.gson.Gson
import junit.framework.Assert.assertEquals
import org.junit.AfterClass
import org.junit.BeforeClass
import org.junit.Test
import java.net.URI
import java.net.http.HttpClient
import java.net.http.HttpRequest
import java.net.http.HttpResponse


class BackendTest {

    companion object {
        @BeforeClass
        @JvmStatic
        fun setUp() = clearDb()

        private fun clearDb() {
            try {
                val process = Runtime.getRuntime().exec("sh src/test/kotlin/testfest/setUpDb.sh")
                process.waitFor()
//                val reader = BufferedReader(InputStreamReader(process.inputStream))
//                val errorReader = BufferedReader(InputStreamReader(process.errorStream))
//                var line: String? = ""
//                while (reader.readLine().also { line = it } != null) println(line)
//                line = ""
//                while (errorReader.readLine().also { line = it } != null) println(line)
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }

        @AfterClass
        @JvmStatic
        fun cleanUp() = clearDb()
    }

    private val gson = Gson()
    private val client = HttpClient.newHttpClient()

    private interface User

    private data class SignUpUser(
        val username: String = "testFest",
        val password: String = "testPassword",
        val firstName: String = "first",
        val middleName: String = "middle",
        val lastName: String = "last",
        val birthDate: String = "date"
    ) : User

    private data class SignInUser(
        val username: String = "hellouser",
        val password: String = "qoQzPBVyTrg3Qdjj"
    ) : User

    private data class FakeSignInUser(
        val username: String = "fake user",
        val password: String = "fake password"
    ) : User

    private fun HttpClient.sendRequest(request: HttpRequest) = send(request, HttpResponse.BodyHandlers.ofString())

    @Test
    fun `success sign up`() {
        val request = createHttpRequest(SignUpUser(), "sign-up")

        val response = client.sendRequest(request)

        assertEquals(201, response.statusCode())
    }

    private fun createHttpRequest(user: User, path: String): HttpRequest {
        val json = gson.toJson(user)
        val body = HttpRequest.BodyPublishers.ofString(json)
        return HttpRequest.newBuilder(URI.create("http://localhost:8080/$path"))
            .POST(body)
            .header("Content-Type", "application/json")
            .build()
    }

    @Test
    fun `failed sign up with login that already exists`() {
        val request = createHttpRequest(SignUpUser(username = "hellouser"), "sign-up")

        val response = client.sendRequest(request)

        assertEquals(401, response.statusCode())
    }

    @Test
    fun `success sign in`() {
        val request = createHttpRequest(SignInUser(), "sign-in")

        val response = client.sendRequest(request)

        assertEquals(200, response.statusCode())
    }

    @Test
    fun `failed sign in`() {
        val request = createHttpRequest(FakeSignInUser(), "sign-in")

        val response = client.sendRequest(request)

        assertEquals(401, response.statusCode())
    }
}
